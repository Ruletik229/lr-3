const toggleButton = document.getElementById('toggleButton');
const dropdownList = document.getElementById('dropdownList');

toggleButton.addEventListener('click', function () {
    if (dropdownList.classList.contains('show')) {
        dropdownList.style.opacity = '0';
        setTimeout(() => {
            dropdownList.classList.remove('show');
        }, 500);
    } else {
        dropdownList.classList.add('show');
        setTimeout(() => {
            dropdownList.style.opacity = '1';
        }, 100);
    }
});

const li = document.querySelectorAll('.dropdown-list > li');

li.forEach((el) => {
    el.addEventListener('click', () => {
        dropdownList.style.opacity = '0';
        setTimeout(() => {
            dropdownList.classList.remove('show');
        }, 500);
    });
});