const formKey = document.getElementById('orderBlock');

document.querySelectorAll('a[href="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        formKey.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
        });
    });
});