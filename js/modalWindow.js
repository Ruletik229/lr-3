document.addEventListener('DOMContentLoaded', () => {
    const modal_window = document.getElementById('modal_window');
    const myPolicy = document.getElementById('myPolicy');
    const closePolicy = document.getElementById(`close`);

    modal_window.addEventListener('click', () => {
        myPolicy.style.display = 'block';
        setTimeout(() => {
            myPolicy.style.opacity = '1'
        }, 200);
    });

    closePolicy.addEventListener(`click`, () => {
        myPolicy.style.opacity = '0'
        setTimeout(() => {
            myPolicy.style.display = 'none'
        }, 200);
    });

    myPolicy.addEventListener(`click`, () => {
        myPolicy.style.opacity = '0'
        setTimeout(() => {
            myPolicy.style.display = 'none'
        }, 200);
    });
});